package personal.scrollview;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

public class Main2Activity extends AppCompatActivity {
    private Button buttonSwitch2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        this.init();
    }

    private void init(){
        this.buttonSwitch2 = (Button) this.findViewById(R.id.button_switch2);
        this.buttonSwitch2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Main2Activity.this.startActivity(new Intent(Main2Activity.this,MainActivity.class));
            }
        });
    }
}
